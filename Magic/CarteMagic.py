from Carte import Carte


class CarteMagic(Carte):

    def __init__(self, cout, mana, illustration, description, attaque=None, pv=None):
        Carte.valeurs = (0, 1, 2, 3, 4, 5, 6)
        Carte.couleurs = ("plaine", "ile", "montagne", "forêt", "marais")

        super().__init__(cout, mana)
        self.__illustration = illustration
        self.__description = description
        self.__attaque = attaque
        self.__pv = pv

    def get_illustration(self):
        return self.__illustration

    def set_illustration(self, illustration):
        self.__illustration = illustration

    illustration = property(get_illustration, set_illustration)

    def get_description(self):
        return self.__description

    def set_description(self, description):
        self.__description = description

    description = property(get_description,set_description)

    def get_attaque(self):
        return self.__attaque

    def set_attaque(self, attaque):
        self.__attaque = attaque

    attaque = property(get_attaque,set_attaque)

    def get_pv(self):
        return self.__pv

    def set_pv(self, pv):
        self.__pv = pv

    pv = property(get_pv, set_pv)

    @staticmethod
    def validation(cout, mana):
        if mana not in Carte.couleurs:
            print("Etreur : {} n'est pas un type de mana valide".format(mana))
            exit(1)
        if cout < 0 or cout > 6:
            print("Erreur : Les valeurs doivent être comprises entre 0 et 6")
            exit(1)

    def __str__(self):
        return "Carte {} : {}".format(self.couleur, self.description)