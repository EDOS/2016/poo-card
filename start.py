from Classique.CarteClassique import CarteClassique
from Classique.Bataille import Bataille
from Joueur import Joueur
from Magic.CarteMagic import CarteMagic

if __name__ == "__main__":
    c = CarteClassique(2, 2)

    j1 = Joueur("John", "Doe")

    jeu = Bataille(j1)
    jeu.demarrer_partie()

    #  Test carte Magic
    #  magic = CarteMagic(5, "plaine", "illustration.png", "Dragon des plaines : description", 5, 10)
    #  print(magic)