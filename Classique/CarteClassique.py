from Carte import Carte


class CarteClassique(Carte):

    def __init__(self, valeur_carte, couleur_carte):
        Carte.valeurs = (None, None, 2, 3, 4, 5, 6 , 7, 8, 9, 10, "Valet", "Dame", "Roi", "As")
        Carte.couleurs = ("Coeur", "Carreau", "Trefle", "Pique")

        super().__init__(valeur_carte, couleur_carte)

    @staticmethod
    def validation(valeur_carte, couleur_carte):
        if valeur_carte < 2 or valeur_carte > 14:
            print("Erreur : la valeur d'une carte est comprise entre 2 et 14")
            exit(1)
        if couleur_carte < 0 or couleur_carte > 3:
            print("Erreur : la couleur d'une carte est comprise entre 0 et 3")
            exit(1)