from Joueur import Joueur
from Classique.JeuClassique import JeuClassique


class Bataille:

    def __init__(self, joueur):
        self.__joueur = joueur
        self.__affichage = input('Afficher les mains (O/N)? :')
        self.__ordinateur = Joueur("RED", "Queen")
        self.__jeu = JeuClassique()

    def demarrer_partie(self):
        self.__melanger()
        self.__distribuer()

        poursuivre = True
        print("Le jeu commence")
        while poursuivre:
            poursuivre = self.__main()

    def __melanger(self):
        self.__jeu.melanger()

    def __distribuer(self):
        print("Jouons avec {} cartes".format(len(self.__jeu.cartes)))
        input('Appuyez sur <Entrée> pour continuer')
        for i in range(len(self.__jeu.cartes)):
            carte = self.__jeu.tirer()
            if i % 2 == 0:
                self.__joueur.ajouter_carte(carte)
            else:
                self.__ordinateur.ajouter_carte(carte)
        print("{} {} a {} cartes, {} {} a {} cartes".format(self.__joueur.prenom, self.__joueur.nom,
                                                            len(self.__joueur.paquet.cartes),
                                                            self.__ordinateur.prenom, self.__ordinateur.nom,
                                                            len(self.__ordinateur.paquet.cartes)
                                                            ))
        afficher_main = input('Afficher les mains distribuées (O/N) ? :')
        if afficher_main == "O":
            print(self.__joueur.paquet.cartes)  # A debugger n'affiche pas les valeurs mais les objets
            print(self.__ordinateur.paquet.cartes)

    def __main(self, reste=[]):
        carte_joueur = self.__joueur.tirer_carte()
        carte_ordinateur = self.__ordinateur.tirer_carte()

        if carte_joueur is None:
            self.__class__.fin_partie(self.__joueur, self.__ordinateur)
            return False
        elif carte_ordinateur is None:
            self.__class__.fin_partie(self.__ordinateur, self.__joueur)
            return False

        self.affiche_console("Main :")
        self.affiche_console("     - {} {} : {}".format(self.__joueur.prenom, self.__joueur.nom, str(carte_joueur)))
        self.affiche_console("     - {} {} : {}".format(self.__ordinateur.prenom, self.__ordinateur.nom, str(carte_ordinateur)))

        if carte_joueur.valeur == carte_ordinateur.valeur:
            reste.append(carte_joueur)
            reste.append(carte_ordinateur)
            return self.__bataille(reste)
        elif carte_joueur.valeur > carte_ordinateur.valeur:
            self.__joueur.ajouter_carte(carte_ordinateur)
            self.__joueur.ajouter_carte(carte_joueur)
            self.affiche_console('    {} {} gagne la main'.format(self.__joueur.prenom, self.__joueur.nom))
        else:
            self.__ordinateur.ajouter_carte(carte_joueur)
            self.__ordinateur.ajouter_carte(carte_ordinateur)
            self.affiche_console('    {} {} gagne la main'.format(self.__ordinateur.prenom, self.__ordinateur.nom))

        return True

    def __bataille(self, reste):
        self.affiche_console("*** BATAILLE ***")
        return self.__main(reste)

    @staticmethod
    def fin_partie(perdant, gagnant):
        perdant.defaites += 1
        gagnant.victoires += 1
        print("{} {} a gagné!!".format(gagnant.prenom, gagnant.nom))

    def affiche_console(self, text):
        if self.__affichage == "O":
            print(text)

