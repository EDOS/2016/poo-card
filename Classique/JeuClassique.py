from JeuCartes import JeuCartes
from Classique.CarteClassique import CarteClassique


class JeuClassique(JeuCartes):
    def __init__(self):
        super().__init__()

    def initialiser(self):
        for valeur in range(2,15):
            for couleur in range(4):
                self.cartes.append(CarteClassique(valeur, couleur))

