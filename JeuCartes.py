import random


class JeuCartes:
    def __init__(self, vide=False):
        if self.__class__ is JeuCartes:
            raise Exception("Creation interdite!")  # Imite le comportement d'une classe dite abstraite
        else:
            self.__cartes = []
            self.initialiser()

    def __get_cartes(self):
        return self.__cartes

    def __set_cartes(self, carte):
        self.__cartes.append(carte)

    cartes = property(__get_cartes, __set_cartes)

    def __str__(self):
        cartes_du_jeu = ""
        for carte in self.cartes:  # Ici self.cartes ne représente pas __cartes mais la propriété carte définie ligne 18
            if cartes_du_jeu == "":
                cartes_du_jeu = str(carte)
            else:
                cartes_du_jeu += ", " + str(carte)
        return cartes_du_jeu

    def melanger(self):
        random.shuffle(self.__cartes)

    def tirer(self, manuel=False):
        try:
            if manuel:
                input('Appuyez sur <Entrée> pour tirer une carte')
            return self.__cartes.pop(0)  # On retire l'élément à l'adresse 0 dans notre liste de cartes
        except IndexError as erreur:
            print("Il n'y a plus de carte dans le paquet")
            return None

    def initialiser(self):
        pass  # Méthode à surcharger dans la classe fille
