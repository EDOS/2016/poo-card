from Paquet import Paquet


class Joueur:

    def __init__(self, nom, prenom):
        self.__nom = nom
        self.__prenom = prenom
        self.__victoire = 0
        self.__defaites = 0
        self.__paquet = Paquet()

    def get_nom(self):
        return self.__nom

    nom = property(get_nom)

    def get_prenom(self):
        return self.__prenom

    prenom = property(get_prenom)

    def get_victoires(self):
        return self.__victoire

    def set_victoires(self, n):
        self.__victoire = n

    victoires = property(get_victoires, set_victoires)

    def get_defaites(self):
        return self.__defaites

    def set_defaites(self, n):
        self.__defaites = n

    defaites = property(get_defaites, set_defaites)

    def get_paquet(self):
        return self.__paquet

    paquet = property(get_paquet)

    def tirer_carte(self):
        return self.paquet.tirer()

    def ajouter_carte(self, carte):
        self.paquet.ajouter(carte)

    def __str__(self):
        return "{} {}\nPalmarès: {} défaite(s) et {} victoire(s)\n{}".format(
            self.prenom, self.nom, self.defaites, self.victoires, str(self.paquet)
        )