class Carte:
    valeurs = None
    couleurs = None

    def __init__(self, valeur_carte, couleur_carte):
        if self.__class__ is Carte:
            raise Exception("Creation interdite!")
        self.__class__.validation(valeur_carte, couleur_carte)
        self.__valeur = valeur_carte
        self.__couleur = couleur_carte

    def get_valeur(self):
        return self.__valeur

    def set_valeur(self, valeur):
        self.__valeur = valeur

    valeur = property(get_valeur, set_valeur)

    def get_couleur(self):
        return self.__couleur

    def set_couleur(self, couleur):
        self.__couleur = couleur

    couleur = property(get_couleur, set_couleur)

    @staticmethod
    def validation(valeur_carte, valeur_couleur):
        pass

    def __str__(self):
        return str(Carte.valeurs[self.valeur]) + " de " + str(Carte.couleurs[self.couleur])

    def affiche(self):
        print(Carte.valeurs[self.__valeur]), "de", Carte.couleurs[self.__couleur]

    def affiche_ascii(self):
        nom = str(Carte.valeurs[self.__valeur]) + " de " + str(Carte.couleurs[self.__couleur])
        taille = len(nom) + 2
        print("/", "-" * taille, "\\", sep="")
        print("|", "-" * taille, "|", sep="")
        print("|\033[1;31m", nom, "\033[1;m|")  # \033[1;30mGray\033[1;m
        print("|", "-" * taille, "|", sep="")
        print("\\", "-" * taille, "/", sep="")